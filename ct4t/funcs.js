// Event Listeners /////////////////////////////////////////////////////////////
$("#process_worksheet").on("click", function () {
  process_worksheet();
});

$("#add_departure_day_row").on("click", function () {
  add_table_row("departure_day");
});

$("#add_return_day_row").on("click", function () {
  add_table_row("return_day");
});

$("#remove_departure_day_row").on("click", function () {
  remove_table_row("departure_day");
});

$("#remove_return_day_row").on("click", function () {
  remove_table_row("return_day");
});

$("#trip_start_date").on("change", function () {
  console.log("#trip_start_date 'on select' fired.");
  let tsd = formatDate(this.valueAsDate);
  console.log("  Got " + tsd);

  $("#depart_day_payperiod_end_date").value = get_pay_period_end_date(this.valueAsDate);
  $('input[name="departure_day_line_date[]"]').each(function () {
    this.value = tsd;
  });
});

$("#trip_end_date").on("change", function () {
  console.log("#trip_end_date 'on select' fired.");

  return_paypd_date = get_pay_period_end_date(this.valueAsDate);
  $("#return_day_payperiod_end_date").value = formatDate(return_paypd_date);
  $('input[name="return_day_line_date[]"]').each(function () {
    this.value = $("#trip_start_date").value;
  });
});

// Functions ///////////////////////////////////////////////////////////////////

function process_worksheet() {
  $('input[name="departure_day_line_day[]"]').each(function () {
    console.log("Processing " + this);
    // Get each sibling date field, calculate the day, and set this field's value to that day.
  });

  console.log(dd_dates);

  days = ["Sun", "Mon", "Tues", "Wed", "Thur", "Fri", "Sat"];

  for (i = 0; i < dd_dates.length; i++) {
    if (dd_dates[i].value != null && dd_dates[i].value !== "") {
      dd_days[i].value = days[dd_dates[i].valueAsDate.getDay()];
    }
  }

  rows = document.querySelector("table#departure_day > tbody > tr");
  console.log(rows);

  // Process Time Elapsed and ComptimeEarned
  for (i = 0; i < rows.length; i++) {
    if (i == 0) {
      continue;
    }
  }
}

function formatDate(date, zone = null) {
  if (zone === null) {
    zone = "UTC";
  }
  dt = luxon.DateTime.fromObject(date, { zone: zone });
  return dt.toISO();
}

function* range(start = 0, end = undefined, step = 1) {
  if (arguments.length === 1) {
    (end = start), (start = 0);
  }

  [...arguments].forEach((arg) => {
    if (typeof arg !== "number") {
      throw new TypeError("Invalid argument");
    }
  });
  if (arguments.length === 0) {
    throw new TypeError("range requires at least 1 argument, got 0");
  }

  if (start >= end) return;
  yield start;
  yield* range(parseInt(start + step), end, step);
}

function add_day_table_header(parent) {
  if (typeof parent == "string") {
    var parent = $("#" + parent);
  } else {
    var parent = $(parent);
  }

  parent.append(
    "<thead><tr><th>Day</th><th>Date</th><th>Local<br />Time</th>" +
      "<th>Local<br />Timezone</th><th>DTZ<br />Time</th><th>Movement</th>" +
      "<th>Location</th><th>Stop<br />Reason</th><th>Elapsed<br />Time</th>" +
      "<th>CT<br />Earned</th><th>Comments</th></tr></thead>"
  );
}

// function add_table_row(table) {
//   t = $("#" + table);
//   rows = t.getElementsByTagName("tr").length;

//   row0 = t.insertRow(rows - 1);
//   cell0 = row0.insertCell(0);
//   cell0.innerHTML = "<input type='text' name='" + table + "_line_day[]' size='4' readonly class='form-control'/>";
//   cell1 = row0.insertCell(1);
//   cell1.innerHTML = "<input type='date' name='" + table + "_line_date[]' class='form-control'/>";
//   cell2 = row0.insertCell(2);
//   cell2.innerHTML = "<input type='time' name='" + table + "_line_localtime[]' class='form-control'/>";
//   cell3 = row0.insertCell(3);
//   cell3.innerHTML = "<td><input type='time' name='" + table + "_line_dtztime[]' class='form-control'/></td>";
//   cell4 = row0.insertCell(4);
//   cell4.innerHTML = "<td>Departed</td>";
//   cell5 = row0.insertCell(5);
//   cell5.innerHTML = "<td><input type='text' name='" + table + "_line_location[]' class='form-control'/></td>";
//   cell6 = row0.insertCell(6);
//   //TODO: Replace with populate_stop_purpose()
//   cell6.innerHTML =
//     "\
//         <select type='select' class='form-control' name='" +
//     table +
//     "_line_stopreason[]'> \
//           <option value='AD' selected>Authorized Delay</option>\
//           <option value='AR'>Authorized Return</option>\
//           <option value='AT'>Awaiting Transportation</option>\
//           <option value='LV'>Leave En Route</option>\
//           <option value='MC'>Mission Complete</option>\
//           <option value='ML'>Meal Time</option>\
//           <option value='TD'>Temporary Duty</option>\
//           <option value='VR'>Voluntary Return</option>\
//         </select>";
//   cell7 = row0.insertCell(7);
//   cell7.innerHTML =
//     "<input type='text' name='" + table + "_line_elapsedtime[]' readonly class='form-control' size='6' />";
//   cell8 = row0.insertCell(8);
//   cell8.innerHTML = "<input type='text' name='" + table + "_line_ctearned[]' readonly class='form-control' size='6'/>";
//   cell9 = row0.insertCell(9);
//   cell9.innerHTML = "<textarea class='form-control' name='" + table + "_line_comments[]'></textarea>";

//   row1 = t.insertRow(rows - 1);
//   cell10 = row1.insertCell(0);
//   cell10.innerHTML = "<input type='text' name='" + table + "_line_day[]' size='4' readonly class='form-control'/>";
//   cell11 = row1.insertCell(1);
//   cell11.innerHTML = "<input type='date' name='" + table + "_line_date[]' class='form-control'/>";
//   cell12 = row1.insertCell(2);
//   cell12.innerHTML = "<input type='time' name='" + table + "_line_localtime[]' class='form-control'/>";
//   cell13 = row1.insertCell(3);
//   cell13.innerHTML = "<td><input type='time' name='" + table + "_line_dtztime[]' class='form-control'/></td>";
//   cell14 = row1.insertCell(4);
//   cell14.innerHTML = "<td>Arrived</td>";
//   cell15 = row1.insertCell(5);
//   cell15.innerHTML = "<td><input type='text' name='" + table + "_line_location[]' class='form-control'/></td>";
//   cell16 = row1.insertCell(6);
//   //TODO: Replace with populate_stop_purpose()
//   cell16.innerHTML =
//     "<select type='select' class='form-control' name='" +
//     table +
//     "_line_stopreason[]'> \
//           <option value='AD' selected>Authorized Delay</option>\
//           <option value='AR'>Authorized Return</option>\
//           <option value='AT'>Awaiting Transportation</option>\
//           <option value='LV'>Leave En Route</option>\
//           <option value='MC'>Mission Complete</option>\
//           <option value='ML'>Meal Time</option>\
//           <option value='TD'>Temporary Duty</option>\
//           <option value='VR'>Voluntary Return</option>\
//         </select>";
//   cell17 = row1.insertCell(7);
//   cell17.innerHTML =
//     "<input type='text' name='" + table + "_line_elapsedtime[]' readonly class='form-control' size='6' />";
//   cell18 = row1.insertCell(8);
//   cell18.innerHTML = "<input type='text' name='" + table + "_line_ctearned[]' readonly class='form-control' size='6'/>";
//   cell19 = row1.insertCell(9);
//   cell19.innerHTML = "<textarea class='form-control' name='" + table + "_line_comments[]'></textarea>";
// }

function remove_table_row(table) {
  t = document.getElementById(table);
  rows = t.getElementsByTagName("tr").length - 1;

  if (rows >= 4) {
    t.deleteRow(2);
    t.deleteRow(2);
  }
}

function get_pay_period_end_date(d) {
  pay_period_end_dates = [
    "1/14/2023",
    "1/28/2023",
    "2/11/2023",
    "2/25/2023",
    "3/11/2023",
    "3/25/2023",
    "4/8/2023",
    "4/22/2023",
    "5/6/2023",
    "5/20/2023",
    "6/3/2023",
    "6/17/2023",
    "7/1/2023",
    "7/15/2023",
    "8/12/2023",
    "8/26/2023",
    "9/9/2023",
    "9/23/2023",
    "10/7/2023",
    "10/21/2023",
    "11/4/2023",
    "11/18/2023",
    "12/2/2023",
    "12/16/2023",
    "12/30/2023",
  ];

  for (i = 0; i < pay_period_end_dates.length; i++) {
    if (pay_period_end_dates[i] > d) {
      return pay_period_end_dates[i];
    } else {
      continue;
    }
  }
}

function populate_stop_purpose(select, selected_option = null) {
  if (typeof select == "string") {
    var parent = $("#" + select);
  } else {
    var parent = $(select);
  }

  var stop_purposes = {
    AD: "Authorized Delay",
    AR: "Authorized Return",
    AT: "Awaiting Transportation",
    LV: "Leave En Route",
    MC: "Mission Complete",
    ML: "Meal Time",
    TD: "Temporary Duty",
    VR: "Voluntary Return",
  };

  $.each(stop_purposes, function (k, v) {
    el = document.createElement("option");
    el.value = k;
    el.text = v;
    if (selected_option !== null && selected_option == k) {
      el.setAttribute("selected", "selected");
    }
    parent.append(el);
  });
}

function populate_day_of_week(select, selected_option = null) {
  if (typeof select == "string") {
    var parent = $("#" + select);
  } else {
    var parent = $(select);
  }

  var days_of_week = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

  $.each(days_of_week, function () {
    el = document.createElement("option");
    el.text = this;
    el.value = this;
    if (selected_option !== null && selected_option == this) {
      el.setAttribute("selected", "selected");
    }
    parent.append(el);
  });
}

function populate_valid_times(select, selected_option = null, type = null) {
  if (typeof select == "string") {
    var parent = $("#" + select);
  } else {
    var parent = $(select);
  }

  var minimum = 0;
  var maximum = 24;

  if (type == "work") {
    minimum = 5;
    maximum = 18;
  }

  var hours = [...range(minimum, maximum)];
  var valid_times = [];

  $.each(hours, function () {
    valid_times.push(("00" + this).slice(-2) + ":00");
    valid_times.push(("00" + this).slice(-2) + ":15");
    valid_times.push(("00" + this).slice(-2) + ":30");
    valid_times.push(("00" + this).slice(-2) + ":45");
  });

  valid_times.push(("00" + maximum).slice(-2) + ":00");

  $.each(valid_times, function () {
    el = document.createElement("option");
    el.text = this;
    el.value = this;
    if (selected_option !== null && selected_option == this) {
      el.setAttribute("selected", "selected");
    }
    parent.append(el);
  });
}

function populate_timezones(select, selected_option = null) {
  if (typeof select == "string") {
    var parent = $("#" + select);
  } else {
    var parent = $(select);
  }

  el = document.createElement("option");
  el.text = "Inherit";
  el.value = null;
  parent.append(el);

  timezones = Intl.supportedValuesOf("timeZone");

  $.each(timezones, function () {
    el = document.createElement("option");
    el.text = this;
    el.value = this;
    if (selected_option !== null && selected_option == this) {
      el.setAttribute("selected", "selected");
    }
    parent.append(el);
  });
}

function import_worksheet(file) {
  //TODO
}
