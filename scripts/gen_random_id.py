#!/usr/bin/env python

import argparse
import ipaddress
from random import randint, choice, SystemRandom
from re import compile
from string import (
  ascii_letters,
  ascii_lowercase,
  ascii_uppercase,
  printable,
  punctuation,
  digits
)
from sys import exit
from uuid import uuid4

parser = argparse.ArgumentParser(
  description="Generate random ID or passphrase with selected pattern"
)
parser.add_argument(
  '--format',
  '-f',
  type=str,
  help="Generate an ID or passphrase in the given format.",
  choices=["UUID", "MAC", "IPv4", "IPv6", "IPv4CIDR", "IPv6CIDR", "Pass"],
  required=True
)

parser.add_argument(
  '--number',
  '-n',
  type=int,
  help="Generate some number of IDs.",
  default=1,
  required=False
)

parser.add_argument(
  '--length',
  '-l',
  type=int,
  help="Password length.",
  default=16,
  required=False
)

parser.add_argument(
  '--complexity',
  '-c',
  type=str,
  help="password complexity.",
  choices=['simple', 'moderate', 'complex'],
  default='moderate'
)

parser.add_argument(
  '--vendor', '-e', type=str, help="Custom MAC Vendor ID", default="00:00:00"
)

try:
  arguments = parser.parse_args()
except:
  parser.print_help()
  exit(1)


def gen_uuid() -> str:
  return uuid4()


def gen_mac(vendor) -> str:
  return f"MAC: {vendor}:%02x:%02x:%02x" % tuple(
    randint(0, 255) for v in range(3)
  )


def gen_ip(ver=4, with_cidr=False) -> str:
  ip = f"IPv{ver}: "
  if ver == 4:
    ip_max = ipaddress.IPv4Address._ALL_ONES
    cidr_min = 16
    cidr_max = 32
    ip += ipaddress.IPv4Address._string_from_ip_int(randint(0, ip_max))
  elif ver == 6:
    ip_max = ipaddress.IPv6Address._ALL_ONES
    cidr_min = 19
    cidr_max = 127
    ip += ipaddress.IPv6Address._string_from_ip_int(randint(0, ip_max))

  if not with_cidr:
    return ip
  else:
    return f"{ip}/{randint(cidr_min, cidr_max)}"


def gen_pass(length: int, complexity: str) -> str:
  ## Complexity requirements:
  ## | case     | length | lower | upper | speical | digits |
  ## |----------|--------|-------|-------|---------|--------|
  ## | simple   | 6      | 3 min | 0     | 0       | 0-6    |
  ## | moderate | 12     | 2 min | 2 min | 0       | 2 min  |
  ## | complex  | 24     | 4 min | 4 min | 4 min   | 4 min  |

  if complexity == 'simple':
    if length < 6:
      length = 6

    passphrase = ''
    passphrase += ''.join(choice(ascii_lowercase) for i in range(3))
    passphrase += ''.join(
      choice([ascii_lowercase, digits]) for i in range(length - 3)
    )
    return passphrase
  elif complexity == 'moderate':
    if length < 12:
      length = 12

    passphrase = ''
    passphrase += ''.join(choice(ascii_lowercase) for i in range(2))
    passphrase += ''.join(choice(ascii_uppercase) for i in range(2))
    passphrase += ''.join(choice(digits) for i in range(2))
    passphrase += ''.join(
      choice([ascii_uppercase, ascii_letters, digits])
      for i in range(length - 6)
    )
    return passphrase
  else:
    if length < 16:
      length = 16
    passphrase = ''
    passphrase += ''.join(choice(ascii_uppercase) for i in range(4))
    passphrase += ''.join(choice(ascii_lowercase) for i in range(4))
    passphrase += ''.join(choice(digits) for i in range(4))
    passphrase += ''.join(choice(punctuation) for i in range(4))

    if length > 16:
      passphrase += ''.join(choice(printable) for i in range(length - 16))

    passphrase = SystemRandom.shuffle(passphrase)
    return passphrase


for i in range(0, arguments.number):
  if (arguments.format == "UUID"):
    print(f"UUID: {gen_uuid()}")
  elif (arguments.format == "MAC"):
    print(gen_mac(arguments.vendor))
  elif (arguments.format == "IPv4"):
    print(gen_ip())
  elif (arguments.format == "IPv6"):
    print(gen_ip(ver=6))
  elif (arguments.format == "IPv4CIDR"):
    print(gen_ip(with_cidr=True))
  elif (arguments.format == "IPv6CIDR"):
    print(gen_ip(ver=6, with_cidr=True))
  elif (arguments.format == "Pass"):
    print(gen_pass(length=arguments.length, complexity=arguments.complexity))

exit(0)