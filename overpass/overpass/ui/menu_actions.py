from PySide6.QtGui import QAction, QIcon, QKeySequence


def add_file_menu(menubar):
  menu = menubar.addMenu("&File")

  new_action = QAction(QIcon(None), "&New Project...", menubar.parent())
  new_action.setStatusTip("Create a new overpass project")
  new_action.setShortcut(QKeySequence.New)
  menu.addAction(new_action)

  open_action = QAction(QIcon(None), "&Open Project...", menubar.parent())
  open_action.setStatusTip("Open an existing overpass project")
  open_action.setShortcut(QKeySequence.Open)
  menu.addAction(open_action)

  save_action = QAction(QIcon(None), "&Save Project...", menubar.parent())
  save_action.setStatusTip("Save this overpass project")
  save_action.setShortcut(QKeySequence.Save)

  quit_action = QAction(QIcon(None), "&Quit", menubar.parent())
  quit_action.setStatusTip("Close overpass")
  quit_action.setShortcut(QKeySequence.Quit)
  quit_action.triggered.connect(menubar.parent().close)
  menu.addAction(quit_action)


def add_edit_menu(menubar):
  menu = menubar.addMenu("Edit")

  rename_action = QAction(QIcon(None), "Rename", menubar.parent())
  menu.addAction(rename_action)

  menu.addSeparator()

  prefs_action = QAction(QIcon(None), "Preferences...", menubar.parent())
  prefs_action.setStatusTip("Edit user preferences")
  prefs_action.setShortcut("CTRL+,")
  menu.addAction(prefs_action)


def add_help_menu(menubar):
  menu = menubar.addMenu("Help")

  help_action = QAction(QIcon(None), "&Help...", menubar.parent())
  help_action.setStatusTip("Get offline help for overpass")
  help_action.setShortcut(QKeySequence.HelpContents)
  menu.addAction(help_action)

  about_action = QAction(QIcon(None), "About Overpass...", menubar.parent())
  about_action.setStatusTip("About overpass")
  menu.addAction(about_action)
