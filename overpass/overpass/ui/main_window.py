from PySide6.QtWidgets import (
  QMainWindow, QWidget, QHBoxLayout, QVBoxLayout, QSizePolicy
)
from PySide6.QtGui import QPalette, QColor

from ui.menu_actions import *


class Color(QWidget):

  def __init__(self, color):
    super(Color, self).__init__()
    self.setAutoFillBackground(True)

    palette = self.palette()
    palette.setColor(QPalette.Window, QColor(color))
    self.setPalette(palette)


class OverpassMainWindow(QMainWindow):

  def __init__(self):
    super(OverpassMainWindow, self).__init__()
    # Setup global application configuration
    self.setWindowTitle("Overpass")
    self.setMinimumSize(1024, 768)

    # Application Menu
    menubar = self.menuBar()
    add_file_menu(menubar)
    add_edit_menu(menubar)
    add_help_menu(menubar)

    # Status Bar
    statsbar = self.statusBar()

    # Main UI Layout
    application_container = QWidget()
    left_container = Color('red')
    left_container.setFixedSize(150, 768)
    left_container.sizePolicy = QSizePolicy(
      QSizePolicy.Maximum, QSizePolicy.MinimumExpanding
    )
    right_container = Color('olive')
    path_and_search_container = Color('grey')
    path_and_search_container.setBaseSize(1024, 50)
    central_container = Color('magenta')
    right_sidebar_container = Color('navy')

    media_browser_widget = Color('pink')
    tag_browser_widget = Color('purple')
    central_media = Color('aquamarine')
    metadata = Color('maroon')
    dock_station_01 = Color('yellow')
    dock_station_02 = Color('yellow')

    application_layout = QHBoxLayout()
    left_container_layout = QVBoxLayout()
    right_container_layout = QVBoxLayout()
    central_container_layout = QHBoxLayout()
    right_sidebar_layout = QVBoxLayout()

    left_container_layout.addWidget(media_browser_widget)
    left_container_layout.addWidget(tag_browser_widget)
    left_container.setLayout(left_container_layout)

    right_sidebar_layout.addWidget(dock_station_01)
    right_sidebar_layout.addWidget(dock_station_02)
    right_sidebar_container.setLayout(right_container_layout)

    # central_container_layout.addWidget(central_media)
    # central_container_layout.addWidget(right_sidebar_container)
    # central_container.setLayout(central_container_layout)

    right_container_layout.addWidget(path_and_search_container)
    right_container_layout.addWidget(central_container)
    right_container_layout.addWidget(metadata)
    right_container.setLayout(right_container_layout)

    application_layout.addWidget(left_container)
    application_layout.addWidget(right_container)
    application_container.setLayout(application_layout)
    self.setCentralWidget(application_container)
    # application_container = QWidget()
    # left_container = QWidget()
    # right_container = QWidget()
    # central_container = QWidget()

    # application_layout = QHBoxLayout()
    # left_sidebar_layout = QVBoxLayout()
    # right_sidebar_layout = QVBoxLayout()
    # path_and_search_layout = QHBoxLayout()
    # central_container_layout = QHBoxLayout()

    # central_widget = Color('green')
    # left_sidebar = Color('yellow')
    # path_and_search = Color('blue')
    # right_sidebar = Color('purple')
    # bottom_bar = Color('red')

    # left_sidebar_layout.addWidget(left_sidebar)
    # left_container.setLayout(left_sidebar_layout)
    # central_container.setLayout(central_container_layout)
    # right_container.setLayout(right_sidebar_layout)
    # application_container.setLayout(application_layout)
    # self.setCentralWidget(application_container)
