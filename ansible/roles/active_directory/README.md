# Active Directory

A basic role to add Windows Active Directory Server services to an existing Windows Server 2016+ server.

## Requirements

A Windows Server installation in a VM or something else that's network accessible.

## Role Variables

A description of the settable variables for this role should go here, including any variables that are in
defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables
that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as
well.

## Dependencies

None (yet).

## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for
users too:

```yaml
- hosts: servers
  roles:
  - { role: username.rolename, x: 42 }
```

## License

MIT

## Author Information

Reddraconi c/o RedSav Studios
[reddraconi@redsavstudios.com](mailto:reddraconi@redsavstudios.com)
