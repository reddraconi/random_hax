#!/usr/bin/env python3

import os
import sys
import string
import logging
import argparse
import concurrent.futures
from typing import List
import xml.etree.ElementTree as ET


class Config:
  """Holds the configuration for the script.

    Attributes:
        source: The source directory to search for files.
        destination: The destination directory for XML files.
        extensions: The list of file extensions to process.
          NOTE: these will always be converted to LOWER CASE
        verbose: The verbosity level for logging.
        processes: The maximum number of parallel processes.
        tag: The XML tag to use for lines.
        max_line_length: The maximum line length in characters.
        max_lines_per_file: The maximum number of 'line' elements per XML file.
        line_prefix: The prefix for continued lines.
    """

  def __init__(
    self,
    source: str = ".",
    destination: str = "xml_output",
    extensions: List[str] = [".txt"],
    verbose: int = 0,
    processes: int = os.cpu_count(),
    tag: str = "line",
    max_line_length: int = 300,
    max_lines_per_file: int = 10000,
    line_prefix: str = "-*-*-*-"
  ):
    self.source = source
    self.destination = destination
    self.extensions = [ext.lower for ext in extensions]
    self.verbose = verbose
    self.processes = processes
    self.tag = tag
    self.max_line_length = max_line_length
    self.max_lines_per_file = max_lines_per_file
    self.line_prefix = line_prefix


def sanitize_line(config: Config, line: str) -> List[str]:
  """Sanitizes a line and splits it if it exceeds confg.max_line_length.

    Sanitizes a line to printable ASCII characters and splits it into
    multiple lines if it exceeds max_line_length, adding line_prefix to
    the start of continued lines. Uses string slicing instead of
    splitting by words.

    Args:
        config: The configuration object.
        line: The line to sanitize.

    Returns:
        A list of sanitized lines.
    """
  try:
    replacements = {
      "&": "&amp;",
      "<": "&lt;",
      ">": "&gt;",
      "'": "&apos;",
      '"': "&quot;",
      "°": "&deg;"
    }
    # Make the content XML safe
    for rep in replacements:
      line = line.replace(rep, replacements[rep])

    # Filter out non-printable characters
    printable = string.printable

    # Jam all of the replacements into a single string
    sanitized = ''.join(filter(lambda x: x in printable, line))

    split_lines = []
    start = 0

    while start < len(sanitized):
      # Split the line into chunks of max_line_length
      end = start + config.max_line_length
      split_lines.append(sanitized[start:end])
      if split_lines:
        # Update the end to remove the prefix length from the previous line
        end -= len(config.line_prefix)
      start = end

    # Add prefix to continued lines (only after the first line)
    if len(split_lines) > 1:
      for i in range(1, len(split_lines)):
        split_lines[i] = config.line_prefix + split_lines[i]

    return split_lines
  except Exception as e:
    logging.error(f"Error sanitizing line: {e}")
    return []


def convert_file_to_xml(input_file: str, config: Config) -> None:
  """Converts a single file to XML.

    Reads the input file, sanitizes the lines, splits them into chunks,
    and creates XML files for each chunk. Handles potential errors
    during file processing.

    Args:
        input_file: The path to the input file.
        config: The configuration object.
    """
  try:
    with open(input_file, 'r', encoding='utf-8', errors='ignore') as f:
      lines = f.readlines()
    logging.info(f"Successfully read file: {input_file}")
  except FileNotFoundError:
    logging.error(f"File not found: {input_file}")
    return
  except Exception as e:
    logging.error(f"Error reading {input_file}: {e}")
    return

  sanitized_lines = []
  for line in lines:
    sanitized_lines.extend(sanitize_line(config, line))

  chunks = [
    sanitized_lines[i:i + config.max_lines_per_file]
    for i in range(0, len(sanitized_lines), config.max_lines_per_file)
  ]

  for chunk_index, chunk in enumerate(chunks):
    try:
      ## TODO: Update XML Namespace to the correct one in OPS
      root = ET.Element("document", xmlns="document.xsd")
      for line in chunk:
        element = ET.SubElement(root, config.tag)
        element.text = line

      tree = ET.ElementTree(root)
      base_filename = os.path.splitext(os.path.basename(input_file))[0]
      output_file = os.path.join(
        config.destination,
        f"{base_filename}_{chunk_index + 1}of{len(chunks)}.xml"
      )

      tree.write(output_file, encoding='utf-8', xml_declaration=True)
      logging.info(
        f"Successfully converted '{input_file}', chunk {chunk_index + 1} to '{output_file}'."
      )
    except Exception as e:
      logging.error(
        f"Error processing chunk {chunk_index + 1} of {input_file}: {e}"
      )


def find_files(config: Config) -> List[str]:
  """Finds files with the specified extensions recursively.

    Args:
        config: The configuration object.

    Returns:
        A list of file paths matching the extensions.
    """
  found_files = []
  for root, _, files in os.walk(config.source):
    for file in files:
      # Make sure any files we find have an allowed extension and are not in the
      # destination directory
      if any(file.lower().endswith(ext) for ext in config.extensions
             ) and os.path.isfile(os.path.join(
               root, file)) and not os.path.startswith(config.destination):
        found_files.append(os.path.join(root, file))
  return found_files


def filter_files(config: Config, files: List[str]) -> List[str]:
  """Filters a list of files by extensions.

    Args:
        config: The configuration object.
        files: The list of files to filter.

    Returns:
        A list of files with the allowed extensions.
    """
  filtered_files = []
  for file in files:
    if any(file.lower().endswith(ext) for ext in config.extensions
           ):  # Use config.extensions
      filtered_files.append(file)
  return filtered_files


if __name__ == "__main__":
  parser = argparse.ArgumentParser(
    description="Convert plaintext files to XML."
  )
  parser.add_argument("-s", "--source", help="Source directory.", default=".")
  parser.add_argument(
    "-d",
    "--destination",
    help="Destination directory.",
    default="./xml_output"
  )
  parser.add_argument(
    "-e",
    "--extensions",
    nargs="+",
    help=
    "File extensions. '.txt' by default. Note: these will always be converted to lowercase.",
    default=[".txt"]
  )
  parser.add_argument(
    "-v",
    "--verbose",
    action="count",
    default=0,
    help="Increase verbosity. Max: -vv"
  )
  parser.add_argument(
    "-p",
    "--processes",
    type=int,
    default=os.cpu_count() // 2,
    help="Max parallel processes. Half of cpu count by default."
  )
  parser.add_argument(
    "-t",
    "--tag",
    type=str,
    default="line",
    help="XML tag for lines. 'line' by default."
  )
  parser.add_argument(
    "-l",
    "--max-line-length",
    type=int,
    default=300,
    help="Maximum line length in characters. 300 by default."
  )
  parser.add_argument(
    "-m",
    "--max-lines-per-file",
    type=int,
    default=10000,
    help="Maximum 'line' elements per XML file. 10000 by default."
  )
  parser.add_argument(
    "-x",
    "--line-prefix",
    type=str,
    default="-*-*-*-",
    help="Prefix for continued lines. '-*-*-*-' by default."
  )

  args = parser.parse_args()

  config = Config(
    source=args.source,
    destination=args.destination,
    extensions=args.extensions,
    verbose=args.verbose,
    processes=args.processes,
    tag=args.tag,
    max_line_length=args.max_line_length,
    max_lines_per_file=args.max_lines_per_file,
    line_prefix=args.line_prefix
  )

  # Set logging verbosity
  log_level = logging.WARNING
  if config.verbose == 1:
    log_level = logging.INFO
  elif config.verbose > 1:
    log_level = logging.DEBUG
  logging.basicConfig(
    level=log_level, format='%(asctime)s - %(levelname)s - %(message)s'
  )

  try:
    input_files = find_files(config)
  except Exception as e:
    logging.error(f"Error finding files: {e}")
    sys.exit(1)

  filtered_files = filter_files(config, input_files)

  if not filtered_files:
    logging.warning(
      f"No files found in '{config.source}' with extensions: {config.extensions}"
    )
    sys.exit(0)

  try:
    if not os.path.exists(config.destination):
      os.makedirs(config.destination)
      logging.info(f"Created output directory: {config.destination}")
  except OSError as e:
    logging.error(f"Error creating output directory: {e}")
    sys.exit(1)

  with concurrent.futures.ProcessPoolExecutor(max_workers=config.processes
                                              ) as executor:
    try:
      futures = [
        executor.submit(convert_file_to_xml, file, config)
        for file in filtered_files
      ]
      concurrent.futures.wait(futures)
    except KeyboardInterrupt:
      logging.warning("Processing interrupted by user!")
      executor.shutdown(wait=False)
      sys.exit(1)
    except Exception as e:
      logging.error(f"Error during concurrent execution: {e}")
      executor.shutdown(wait=False)
      sys.exit(1)

  logging.info("Processing complete.")
  exit(0)
