#!/usr/bin/env python3

import os
import sys
import logging
import argparse
from typing import List
import xml.etree.ElementTree as ET
import concurrent.futures


class Config:
  """Holds the configuration for the script.

    Attributes:
        source: The source directory to search for files.
        destination: The destination directory for reconstructed files.
        verbose: The verbosity level for logging.
        processes: The maximum number of parallel processes.
        tag: The XML tag to use for lines.
        line_prefix: The prefix for continued lines.
    """

  def __init__(
    self,
    source: str = ".",
    destination: str = "xml_output",
    verbose: int = 0,
    processes: int = os.cpu_count(),
    tag: str = "line",
    line_prefix: str = "-*-*-*-"
  ):
    self.source = source
    self.destination = destination
    self.verbose = verbose
    self.processes = processes
    self.tag = tag
    self.line_prefix = line_prefix


def convert_xml_to_text(input_file: str, config: Config) -> None:
  """Converts a single XML file back to plaintext.

  Reads the XML file, extracts the text content from the 'line' elements,
  unescapes XML entities, reconstructs long lines that were split, and
  writes the output to a plaintext file.

  Args:
      input_file: Path to the input XML file.
      output_dir: The directory to write the converted plaintext file to.
      line_prefix: The prefix used for continued lines in the original files.
  """
  try:
    tree = ET.parse(input_file)
    root = tree.getroot()
    lines = [elem.text for elem in root.findall(".//" + config.tag)]

    # Unescape XML entities
    replacements = {
      "&amp;": "&", "&lt;": "<", "&gt;": ">", "&quot;": "\"", "&apos;": "'"
    }

    for entity, char in replacements.items():
      lines = [line.replace(entity, char) for line in lines]

    # Reconstruct long lines
    reconstructed_lines = []
    current_line = ""
    for line in lines:
      if line.startswith(config.line_prefix):
        current_line += line[len(config.line_prefix):]
      else:
        if current_line:
          reconstructed_lines.append(current_line)
        current_line = line
    if current_line:
      reconstructed_lines.append(current_line)

    # Determine original filename based on naming pattern _1ofN
    # If there are multiple parts, they'll all get concatenated in order, based
    # on the sorting of the filenames in 'input_files'.
    base_filename = input_file.split("_1of")[0]
    output_file = os.path.join(
      config.output_dir, os.path.basename(base_filename)
    )

    with open(output_file, 'w', encoding='utf-8') as f:
      f.write('\n'.join(reconstructed_lines))
    logging.info(f"Successfully converted '{input_file}' to '{output_file}'")

  except FileNotFoundError:
    logging.error(f"File not found: {input_file}")
  except Exception as e:
    logging.error(f"Error processing {input_file}: {e}")


def check_file_completeness(input_files: List[str]) -> List[str]:
  """Checks for file completeness based on the _xofN naming pattern.

    Args:
        input_files: List of input XML file paths.

    Returns:
        A list of complete XML files with all their parts present.
    """
  complete_files = []
  for file in input_files:
    try:
      # Extract file number and total files from filename
      parts = file.split("_")[-1]
      if len(parts) == 2:
        xofn = parts[1].split(".xml")[0]
        _, n = map(int, xofn.split("of"))

        # Check if all parts of the file exist
        all_parts_exist = all(
          os.path.exists(f"{parts[0]}_{i}of{n}.xml") for i in range(1, n + 1)
        )
        if all_parts_exist:
          complete_files.append(file)
        else:
          logging.warning(f"Skipping incomplete file: {file}")
      else:
        logging.warning(f"Skipping file with invalid name: {file}")
    except (ValueError, IndexError) as e:
      logging.warning(f"Skipping file with invalid name: {file} - {e}")
  return complete_files


if __name__ == "__main__":
  parser = argparse.ArgumentParser(
    description="Convert XML files back to plaintext."
  )
  parser.add_argument("input_files", nargs="+", help="Input XML files.")
  parser.add_argument(
    "-d",
    "--destination",
    help="Destination directory for output files.",
    default="text_output"
  )
  parser.add_argument(
    "-v", "--verbose", action="count", default=0, help="Increase verbosity."
  )
  parser.add_argument(
    "-x",
    "--line-prefix",
    type=str,
    default="",
    help="Prefix for continued lines."
  )
  parser.add_argument(
    "-t",
    "--threads",
    type=int,
    default=os.cpu_count(),
    help="Number of threads to use for parallel processing."
  )

  args = parser.parse_args()

  # Set logging level based on verbosity requested by the user
  log_level = logging.WARNING
  if args.verbose == 1:
    log_level = logging.INFO
  elif args.verbose > 1:
    log_level = logging.DEBUG
  logging.basicConfig(
    level=log_level, format='%(asctime)s - %(levelname)s - %(message)s'
  )

  if not os.path.exists(args.destination):
    try:
      os.makedirs(args.destination)
      logging.info(f"Created output directory: {args.destination}")
    except OSError as e:
      logging.error(f"Error creating output directory: {e}")
      sys.exit(1)

  complete_files = check_file_completeness(args.input_files)

  with concurrent.futures.ThreadPoolExecutor(max_workers=args.threads
                                             ) as executor:
    futures = [
      executor.submit(
        convert_xml_to_text, file, args.destination, args.line_prefix
      ) for file in args.complete_files
    ]
    concurrent.futures.wait(futures)

  logging.info("All files processed.")
  exit(0)
