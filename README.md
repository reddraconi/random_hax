# random_hax

A container to hold random things that don't have a dedicated project.

## Name

Random hacks.

## Description

This is simply a container for random things that don't have a parent project.

## Contributing

I'm always happy to receive pull requests!

### Codestyle

#### Bash

I highly recommend you run `shellcheck` against any shell scripts. It's handy
and able to find a lot of pitfalls before they become black holes.

#### Python

I use `yapf` to format my python code. There's a `.style.yapf` file in this
repo that you can use to match my formatting. **Please do that before submitting
a pull request.**

If you don't have yapf, you can run these commands to create a virtualenv for
your version of Python, then activate your new virutal environment, and use pip
to install yapf. (This document assumes you're running Linux. Other OSes may
have different instructions.)

```.bash
python -m venv .venv
source .venv/bin/activate
python -m pip install pip --upgrade
python -m pip install yapf
```

## Authors and acknowledgment

Everything in here (so far) has been written by Reddraconi.

## License

Stuff in this repo is covered by the MIT license. Any contributions to this code
must adhere to the same.

## Project status

Ongoing and sporadic
