#!/usr/bin/env python

from random import random
from math import floor
from turtle import down
from requests import get as req_get, head as req_head
from requests.exceptions import HTTPError as httperror
from sys import argv
import threading

max_retries = 5

def download_fragment(start_byte:int, stop_byte:int, url:str, container):

  req_header = {
    'Accept': 'text/css,*/*;q=0.1',
    'Accept-Language': 'en-US,en:q=0.5',
    'Accept-Encoding': 'gzip, deflate, br, identity',
    'Connection': 'keep-alive',
    'DNT': "1",
    'Host': url.split("//")[-1].split("/")[0].split('?')[0],
    'Range': f"bytes={start_byte}-{stop_byte}"
  }

  try:
    req = req_get(url, headers=req_header, stream=True)
    req.raise_for_status()
    container = req.content
  except httperror as e:
    raise e
  except Exception as e:
    raise e


def threaded_downloader(url:str, file_path:str, num_threads:int = 5) -> bool:
  """ Chunks and downloads files
  
  Args:
    url (str): The URL Target to nab
    file_path (str): Where you want the final output to go. Set to "None" for
      an in-memory buffer
    threads (int): The number of threads you want to use. 5 by default. Each
      thread gets an evenly-ish sized chunk of the file to download.
  
  Returns:
    True when the file or in-memory buffer is complete or False if the timeout
    value and retries are expended

  Raises:
    IOException: Occurs if the file or buffer couldn't be written
    HTTPError: Occurs if any 400 or 500-class HTTP error occurs
  """
  req = req_head(url)
  print(req)
  file_name = url.split('/')[-1]
  download_container = {}

  try:
    total_bytes = int(req.headers['content-length'])
    fragment_size = floor(total_bytes / num_threads)
  except Exception as e:
    raise e


  for i in range(num_threads):
    start_byte = i * fragment_size
    stop_byte = start_byte + fragment_size

    if i == num_threads:
      stop_byte = start_byte + (total_bytes - start_byte)

    download_container[i] = None

    thread = threading.Thread(
      target=download_fragment,
      kwargs={
        'start_byte': start_byte,
        'stop_byte': stop_byte,
        'url': url,
        'container': download_container[i]
      }
    )
    thread.setDaemon(True)
    thread.start()

  controller = threading.current_thread()
  for t in threading.enumerate():
    if t is controller:
      continue
    t.join()

  print(f"{file_name} download complete.")


if __name__ == "__main__":

  url = argv[1]
  destination = argv[2]
  threaded_downloader(url, destination)
