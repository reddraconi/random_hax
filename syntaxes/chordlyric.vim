" Vim syntax file
" Language: Custom guitar tabulation + lyric file
" Mantainer: Reddraconi
" Lastest Revision: 20230609

if exists("b:current_syntax")
  finish
endif

" Keywords

syntax keyword cl_todos TODO FIXME FIXUP NOTE contained
syntax match cl_header '^#chordlyric'
syntax match cl_chord '^\..*$'
syntax match cl_comment '// .*$' contains=cl_todos

" Regions

" Highlight Links

let b:current_syntax = "chordlyric"
highlight def link cl_chord PreProc
highlight def link cl_header Comment
highlight def link cl_sectionmarker Comment
highlight def link cl_comment Comment
